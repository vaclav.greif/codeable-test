<?php

namespace WPProgramator\Codeable\Test;

/**
 * Class Frontend
 * @package WPProgramator\CA_TEST
 */
class Frontend {
	/**
	 * @var Users
	 */
	private $users;

	/**
	 * Frontend constructor.
	 *
	 * @param Users $users
	 */
	public function __construct( Users $users ) {
		$this->users = $users;

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_scripts' ) );
		add_shortcode( 'codeable_test_table', array( $this, 'render_table' ) );
		add_action( 'init', array( $this, 'load_textdomain' ) );
	}

	/**
	 * Enqueue frontend scripts
	 */
	public function enqueue_frontend_scripts() {
		wp_enqueue_script( 'ca-test', CA_TEST_URL . 'dist/js/bundle.js', [ 'jquery' ], CA_TEST_VERSION, true );
		wp_localize_script(
			'ca-test',
			'caTestSettings',
			[
				'nonce' => wp_create_nonce( 'wp_rest' ),
			]
		);

		wp_enqueue_style( 'ca-test', CA_TEST_URL . 'dist/css/app.css', [], CA_TEST_VERSION );
	}

	/**
	 * Render the users table
	 */
	public function render_table() {
		// Only admin users allowed to view the table
		if ( ! current_user_can( 'administrator' ) ) {
			return '';
		}

		return $this->get_table();
	}

	/**
	 * Get the table HTML
	 *
	 * @param \WP_User_Query|null $users
	 * @param bool $including_filters
	 *
	 * @return false|string
	 */
	public function get_table( \WP_User_Query $users = null, $including_filters = true ) {
		ob_start();
		if ( null === $users ) {
			$users = $this->users->get_users();
		}
		$roles = $this->users->get_editable_roles();

		// Define the table columns
		$columns = [
			'email'        => [
				'label'    => __( 'Email', 'codeable-test' ),
				'property' => 'user_email',
			],
			'username'     => [
				'label'    => __( 'Username', 'codeable-test' ),
				'property' => 'user_login',
			],
			'first_name'   => [
				'label'    => __( 'First Name', 'codeable-test' ),
				'property' => 'first_name',
			],
			'last_name'    => [
				'label'    => __( 'Last Name', 'codeable-test' ),
				'property' => 'last_name',
			],
			'display_name' => [
				'label'    => __( 'Display Name', 'codeable-test' ),
				'property' => 'display_name',
			],
			'role'         => [
				'label'    => __( 'Role', 'codeable-test' ),
				'property' => 'role',
			],
		];
		?>
        <div class="ca-test-table">
			<?php if ( $including_filters ) { ?>
                <div class="ca-filters">
                    <form action="<?php echo ca_test_get_rest_url() ?>/users" method="get" class="ca-filters-form">
                        <label for="ca-orderby"><?php _e( 'Order by', 'codeable-test' ); ?></label>
                        <select name="orderby" id="ca-orderby">
                            <option value="display_name"><?php _e('Display name','codeable-test'); ?></option>
                            <option value="user_login"><?php _e('Username','codeable-test'); ?></option>
                        </select>
                        <label for="ca-order"><?php _e( 'Order', 'codeable-test' ); ?></label>
                        <select name="order" id="ca-order">
                            <option value="ASC"><?php _e( 'Ascending', 'codeable-test' ); ?></option>
                            <option value="DESC"><?php _e( 'Descending', 'codeable-test' ); ?></option>
                        </select>
                        <label for="ca-role"><?php _e( 'Filter by role', 'codeable-test' ); ?></label>
                        <select name="role" id="ca-role">
                            <option value=""><?php _e( 'Select role', 'codeable-test' ); ?></option>
							<?php foreach ( $roles as $key => $role ) { ?>
                                <option value="<?php echo $key; ?>"><?php echo $role['name']; ?></option>
							<?php } ?>
                        </select>
                        <div class="pagination-wrapper">
							<?php echo $this->get_pagination( $users ); ?>
                        </div>
                        <input type="hidden" name="page" value="1"/>
                    </form>
                </div>
			<?php } ?>

            <div class="ca-table">
                <table>
                    <thead>
                    <tr>
						<?php foreach ( $columns as $column ) { ?>
                            <th><?php echo $column['label'] ?></th>
						<?php } ?>
                    </tr>
                    </thead>
                    <tbody>
					<?php
                    if ( ! empty( $users->get_results() ) ) {
						foreach ( $users->get_results() as $user ) { ?>
                            <tr>
								<?php foreach ( $columns as $column ) { ?>
                                    <td>
										<?php
										if ( 'role' === $column['property'] ) {
										    // User can have more roles, display all of them
											$r = [];
											foreach ( $user->roles as $role ) {
												$r[] = $roles[ $role ]['name'];
											}
											echo implode( ', ', $r );
										} else {
											echo $user->{$column['property']};
										}
										?>
                                    </td>
								<?php } ?>
                            </tr>
						<?php }
					} else { ?>
                        <tr>
                            <td colspan="<?php echo count( $columns ); ?>"><?php _e( 'No users found.', 'codeable-test' ); ?></td>
                        </tr>
						<?php

					}
					?>
                    </tbody>
                </table>
            </div>
			<?php if ( $including_filters ) { ?>
                <div class="pagination-wrapper">
					<?php echo $this->get_pagination( $users ); ?>
                </div>
			<?php } ?>
        </div>
		<?php
		return ob_get_clean();
	}

	/**
	 * Get the pagination HTML
	 *
	 * @param \WP_User_Query $users
	 *
	 * @return false|string
	 */
	public function get_pagination( \WP_User_Query $users ) {
		$current_page = (int) $users->query_vars['paged'] ?: 1;
		$total_users  = $users->get_total();
		ob_start();
		?>
        <div class="pagination">
			<?php
			// Count the total number of pages
			$num_pages = ceil( $total_users / CA_USERS_PER_PAGE );
			for ( $i = 1; $i <= $num_pages; $i ++ ) { ?>
                <a href="#" data-page="<?php echo $i; ?>" class="<?php echo $current_page === $i ? 'current' : ''; ?>"><?php echo $i; ?></a>
			<?php } ?>
        </div>
		<?php
		return ob_get_clean();
	}

	/**
	 * Load the textdomain
	 */
	public function load_textdomain() {
		load_plugin_textdomain( 'codeable-test', false, CA_TEST_DIR . 'languages' );
	}
}
