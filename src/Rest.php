<?php

namespace WPProgramator\Codeable\Test;

/**
 * Class Rest
 * @package WPProgramator\CA_TEST
 */
class Rest extends \WP_REST_Controller {
	/**
	 * @var Users
	 */
	private $users;
	/**
	 * @var Frontend
	 */
	private $frontend;

	/**
	 * Rest constructor.
	 *
	 * @param Users $users
	 * @param Frontend $frontend
	 */
	public function __construct( Users $users, Frontend $frontend ) {
		add_action( 'rest_api_init', array( $this, 'register_routes' ) );
		$this->users    = $users;
		$this->frontend = $frontend;
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {

		register_rest_route(
			CA_TEST_REST_NAMESPACE,
			'users',
			array(
				array(
					'methods'  => \WP_REST_Server::READABLE,
					'callback' => array( $this, 'get_users' ),
				),
			)
		);
	}

	/**
	 * Shorten URL
	 *
	 * @param \WP_REST_Request $request Full data about the request.
	 *
	 * @return \WP_Error|\WP_REST_Request|\WP_REST_Response | bool
	 */
	public function get_users( $request ) {
		if ( ! is_user_logged_in() ) {
			return new \WP_Error( 'not-authenticated', __( 'You are not authenticated', 'codeable-test' ), [ 'code' => 501 ] );
		}

		// Setup the filters if requested to
		$args            = [];
		$args['orderby'] = $request->get_param( 'orderby' ) ?: 'display_name';
		$args['order']   = $request->get_param( 'order' ) ?: 'ASC';
		$args['paged']   = $request->get_param( 'page' ) ?: 1;
		$args['role']    = $request->get_param( 'role' ) ?: '';

		// Get the users
		$users = $this->users->get_users( $args );
		// Get the table HTML
		$table = $this->frontend->get_table( $users, false );
		// Get pagination
		$pagination = $this->frontend->get_pagination( $users );

		return new \WP_REST_Response(
			array(
				'status'     => 'success',
				'table'      => $table,
				'pagination' => $pagination,
			),
			200
		);
	}
}
