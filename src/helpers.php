<?php
/**
 * Get rest URL
 * @return string
 */
function ca_test_get_rest_url() {
	return rest_url( CA_TEST_REST_NAMESPACE );
}