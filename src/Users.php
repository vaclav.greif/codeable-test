<?php

namespace WPProgramator\Codeable\Test;

class Users {
	/**
	 * Get the users
	 *
	 * @param array $args
	 *
	 * @return \WP_User_Query
	 */
	public function get_users( $args = [] ) {
		$defaults = [
			'number'  => CA_USERS_PER_PAGE,
			'orderby' => 'display_name',
			'order'   => 'ASC',
			'paged'   => 1,
			'role'    => '',
		];

		$args = wp_parse_args( $args, $defaults );

		$user_query = new \WP_User_Query( $args );

		return $user_query;
	}

	/**
	 * Get available roles
	 * @return mixed
	 */
	public function get_editable_roles() {
		global $wp_roles;

		$all_roles      = $wp_roles->roles;
		$editable_roles = apply_filters( 'editable_roles', $all_roles );

		return $editable_roles;
	}
}
