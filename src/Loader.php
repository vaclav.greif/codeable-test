<?php

namespace WPProgramator\Codeable\Test;

/**
 * Class Loader
 * @package WPProgramator\CA_TEST
 */
class Loader {
	/**
	 * @var Users
	 */
	public $users;

	/**
	 * @var Rest
	 */
	public $rest;

	/**
	 * @var Frontend
	 */
	public $frontend;


	/**
	 * Loader constructor.
	 */
	public function __construct() {
		$this->users    = new Users();
		$this->frontend = new Frontend( $this->users );
		$this->rest     = new Rest( $this->users, $this->frontend );
	}
}
