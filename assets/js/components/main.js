jQuery(document).ready(function ($) {
    let caTest = {
        table: $('.ca-test-table'),
        filtersForm: $('.ca-filters-form'),
        paginationLinks: $('.pagination a'),
        pageInput: $('input[name=page]'),
        init: function () {
            $('select[name="orderby"], select[name="order"]', caTest.table).on('change', caTest.orderbyChanged);
            $('select[name="role"]', caTest.table).on('change', caTest.roleChanged);
            $('body').on('click', '.pagination a', caTest.paginationClick);
        },
        orderbyChanged: function() {
          caTest.resetPage();
          caTest.getUsers();
        },
        roleChanged: function() {
            caTest.resetPage();
            $('select[name="orderby"]').val('display_name');
            $('select[name="order"]').val('ASC');
            caTest.getUsers();
        },
        resetPage: function() {
            caTest.pageInput.val(1);
        },
        getUsers: function () {
            $.ajax({
                url: caTest.filtersForm.attr('action'),
                method: caTest.filtersForm.attr('method'),
                data: caTest.filtersForm.serialize(),
                beforeSend: function ( xhr ) {
                    xhr.setRequestHeader( 'X-WP-Nonce', caTestSettings.nonce );
                },
                success: function (response) {
                    $('.ca-table',caTest.table).html(response.table);
                    $('.pagination-wrapper',caTest.table).html(response.pagination);
                }
            })
        },
        paginationClick: function(e) {
            e.preventDefault();
            caTest.paginationLinks.removeClass('current');
            $(this).addClass('current');
            caTest.pageInput.val($(this).data('page'));
            caTest.getUsers();
        }
    };
    caTest.init();
});