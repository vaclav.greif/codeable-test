<?php
/*
Plugin Name: Codeable Test
Description: This is a test plugin for Codeable
Author: Václav Greif
Author URI: https://wp-programator.cz/
Version: 1.0
Textdomain: codeable-test
*/

namespace WPProgramator\Codeable\Test;

DEFINE( 'CA_TEST_VERSION', '1.0' );
DEFINE( 'CA_TEST_URL', plugins_url( '/', __FILE__ ) );
define( 'CA_TEST_DIR', plugin_dir_path( __FILE__ ) );
define( 'CA_TEST_PREFIX', '_ca_test_' );
define( 'CA_TEST_REST_NAMESPACE', 'ca-test/v1' );
define( 'CA_USERS_PER_PAGE', 10 );

require_once 'vendor/autoload.php';
require_once 'src/helpers.php';

/**
 * Get the container
 */
function wpp_ca_test() {
	static $container;
	if ( ! $container ) {
		$container = new Loader();
	}

	return $container;
}

wpp_ca_test();
